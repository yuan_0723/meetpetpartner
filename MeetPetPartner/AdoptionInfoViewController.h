//
//  AdoptionInfoViewController.h
//  MeetPetPartner
//
//  Created by Yvonne.H on 2017/2/20.
//  Copyright © 2017年 YvonneH. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AdoptionInfoViewController : UIViewController

@property NSDictionary *adoptionInfo;	// 用來接收傳值過來的寵物資訊字典

@end
