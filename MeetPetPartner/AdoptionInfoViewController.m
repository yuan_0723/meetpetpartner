//
//  AdoptionInfoViewController.m
//  MeetPetPartner
//
//  Created by Yvonne.H on 2017/2/20.
//  Copyright © 2017年 YvonneH. All rights reserved.
//
#import "DataManager.h"
#import <UIImageView+WebCache.h>
#import "AdoptionInfoViewController.h"
#import "PetInfoTableViewController.h"
#import "PetContactTableViewController.h"

@interface AdoptionInfoViewController () <UIGestureRecognizerDelegate> {

	PetInfoTableViewController *petInfoChildView;
	PetContactTableViewController *petContactChildView;
	DataManager *dataManager;
	NSMutableArray *userFavorite;
}

@property (weak, nonatomic) IBOutlet UIView *petInfoView;
@property (weak, nonatomic) IBOutlet UIView *petContactView;
@property (weak, nonatomic) IBOutlet UIImageView *petPhoto;
@property (weak, nonatomic) IBOutlet UIButton *addFavoriteBtn;
@property (weak, nonatomic) IBOutlet UIButton *deleteFavoriteBtn;

@end

@implementation AdoptionInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
	
	UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
								   initWithTitle:@""
								   style:UIBarButtonItemStylePlain
								   target:nil
								   action:nil];
	self.navigationItem.backBarButtonItem = backButton;
	
	// 載入時預設先隱藏領養聯絡資訊container
	_petContactView.hidden = true;
	
	// 將要呈現的資訊傳遞給子物件
	[self passDataToContainerView:_adoptionInfo];
	
	// 載入單例物件
	dataManager = [DataManager sharedInstance];
	[dataManager loadSetting];
	userFavorite = [NSMutableArray new];
}

-(void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
	
	userFavorite = dataManager.userFavoriteAdoption;
	
	if ([userFavorite containsObject:_adoptionInfo[PET_ID]]) {
		_addFavoriteBtn.hidden = true;
		_deleteFavoriteBtn.hidden = false;
	} else {
		_addFavoriteBtn.hidden = false;
		_deleteFavoriteBtn.hidden = true;
	}
	
	// 設定照片顯示
	if ([_adoptionInfo[PET_PHOTO_URL] isEqualToString:@""]) {
		
		if ([_adoptionInfo[PET_KIND] isEqualToString:PET_KIND_INFO[0]]) {
			_petPhoto.image = [UIImage imageNamed:@"CatNoImage"];
		} else {
			_petPhoto.image = [UIImage imageNamed:@"DogNoImage"];
		}
		
	} else {
		
		NSURL *imgURL = [NSURL URLWithString:_adoptionInfo[PET_PHOTO_URL]];
		[_petPhoto sd_setImageWithURL:imgURL placeholderImage:[UIImage imageNamed:@"ImageLoading"]];
		
	}
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
	[dataManager deleteSDWebImageCache];
}

#pragma mark - IBAction Methods

/// 分享浪浪領養資訊
- (IBAction)shareBtnPressed:(UIBarButtonItem *)sender {
  
	// 準備認養資訊
  NSString *sharePetID = [NSString stringWithFormat:@"%d", [_adoptionInfo[PET_ID] intValue]];
	NSString *sharePetKind = [DataManager showUserPetKindInfo:_adoptionInfo[PET_KIND]];
	NSString *sharePetGender = [DataManager showUserPetGender:_adoptionInfo[PET_GENDER]];
	NSString *sharePetBodyType = [DataManager showUserBodyTypeInfo:_adoptionInfo[PET_BODY_SIZE]];
	NSString *sharePetAge = [DataManager showUserAgeInfo:_adoptionInfo[PET_AGE]];
	NSString *sharePetSterilization = [DataManager showUserSterilizationInfo:_adoptionInfo[PET_STERILIZATION]];
	NSString *sharePetBacterin = [DataManager showUserBacterinInfo:_adoptionInfo[PET_BACTERIN]];
	NSString *sharePetClour = _adoptionInfo[PET_COLOUR];
	NSString *sharePetShelter = _adoptionInfo[PET_SHELTER];
	NSString *sharePetInfo = [NSString stringWithFormat:@"我在浪浪回家APP看到一隻可愛的浪浪！ \n \n *浪浪編號：%@ \n *浪浪類型：%@ \n *浪浪性別：%@ \n *浪浪體型：%@ \n *浪浪毛色：%@ \n *浪浪年紀：%@ \n *是否絕育：%@ \n *是否施打疫苗：%@ \n *所在收容所：%@ \n \n %@",sharePetID, sharePetKind, sharePetGender, sharePetBodyType, sharePetClour, sharePetAge, sharePetSterilization, sharePetBacterin, sharePetShelter, APP_SHARE_URL];
	UIImage *shareImage = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:_adoptionInfo[PET_PHOTO_URL]]]];
	
	dispatch_async(dispatch_get_main_queue(), ^{
		NSArray *activityItems=@[shareImage,sharePetInfo];
		UIActivityViewController *viewController=[[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
		
		// 過濾不想呈現在activity內的項目
		viewController.excludedActivityTypes=@[UIActivityTypePrint,UIActivityTypeCopyToPasteboard,UIActivityTypeSaveToCameraRoll,UIActivityTypeAssignToContact];
		
		[self presentViewController:viewController animated:YES completion:nil];
	});
}

/// 按下收藏按鈕後觸發的方法
- (IBAction)addFavoriteBtnPressed:(UIButton *)sender {
	[userFavorite addObject:_adoptionInfo[PET_ID]];
	[dataManager writeSetting:userFavorite forKey:USER_SETTING_FAVORITE];
	
	dispatch_async(dispatch_get_main_queue(), ^{
		_addFavoriteBtn.hidden = true;
		_deleteFavoriteBtn.hidden = false;
	});
}

/// 按下取消收藏後觸發的方法
- (IBAction)deleteFavoriteBtnPressed:(UIButton *)sender {
	[userFavorite removeObject:_adoptionInfo[PET_ID]];
	[dataManager writeSetting:userFavorite forKey:USER_SETTING_FAVORITE];
	
	dispatch_async(dispatch_get_main_queue(), ^{
		_addFavoriteBtn.hidden = false;
		_deleteFavoriteBtn.hidden = true;
	});
}

#pragma mark - Private Methods
- (IBAction)infoTypeChanged:(id)sender {
	//利用選項Index來判斷要呈現的cotainer
	switch ([sender selectedSegmentIndex]) {
		case 0:
			_petInfoView.hidden = false;
			_petContactView.hidden = true;
			break;
			
		case 1:
			_petInfoView.hidden = true;
			_petContactView.hidden = false;
			break;
			
		default:
			_petInfoView.hidden = false;
			_petContactView.hidden = true;
			break;
	}
}

// 傳值到containerView
- (void)passDataToContainerView:(NSDictionary *)petAdoptInfo {
	petContactChildView = self.childViewControllers[0];
	petInfoChildView = self.childViewControllers[1];
	
	petContactChildView.adoptionInfo = petAdoptInfo;
	petInfoChildView.adoptionInfo = petAdoptInfo;
}

//-(UIImage *)captureScreenInRect:(CGRect)captureFrame
//{
//	CALayer *layer;
//	layer = self.view.layer;
//	UIGraphicsBeginImageContext(self.view.bounds.size);
//	CGContextClipToRect (UIGraphicsGetCurrentContext(),captureFrame);
//	[layer renderInContext:UIGraphicsGetCurrentContext()];
//	UIImage *screenImage = UIGraphicsGetImageFromCurrentImageContext();
//	UIGraphicsEndImageContext();
//	return screenImage;
//}

@end
