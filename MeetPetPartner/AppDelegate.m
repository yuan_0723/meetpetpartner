//
//  AppDelegate.m
//  MeetPetPartner
//
//  Created by Yvonne.H on 2017/2/16.
//  Copyright © 2017年 YvonneH. All rights reserved.
//

#import "AppDelegate.h"
#import "Reachability.h"

@interface AppDelegate () {
	Reachability *reachabilty;
}

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
	
	// 監聽網路狀態
	[[NSNotificationCenter defaultCenter] addObserver:self selector: @selector(reachabilityChanged:) name:kReachabilityChangedNotification object:nil];
	reachabilty = [Reachability reachabilityForInternetConnection];
	[reachabilty startNotifier];
 
	return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
	// Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
	// Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
	// Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
	// If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
	// Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
	// Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
	// Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


#pragma mark - Reachability Methods

/// 當網路狀態被改變時呼叫的方法
- (void)reachabilityChanged:(NSNotification *)note
{
	Reachability* curReach = [note object];
	NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
	[self updateInterfaceWithReachability: curReach];
}

/// 網路斷線時要觸發的行為
- (void)updateInterfaceWithReachability:(Reachability *)curReach
{
	NetworkStatus curStatus;
 
	BOOL m_bReachableViaWWAN;
	BOOL m_bReachableViaWifi;
	curStatus = [curReach currentReachabilityStatus];
 
	// 判斷網路狀況
	if (curStatus == ReachableViaWWAN) {
		m_bReachableViaWWAN = true;
	} else {
		m_bReachableViaWWAN = false;
	}
 
	if (curStatus == ReachableViaWiFi) {
		m_bReachableViaWifi = true;
	} else {
		m_bReachableViaWifi = false;
	}
	
	//當裝置無法連線時彈出視窗提醒
	if (!(m_bReachableViaWifi || m_bReachableViaWWAN)) {
		dispatch_async(dispatch_get_main_queue(), ^{
			UIAlertController * alert = [UIAlertController alertControllerWithTitle:@"注意！"
																			message:@"手機裝置目前無法連結網路"
																	 preferredStyle:UIAlertControllerStyleAlert];
			UIAlertAction * confirm = [UIAlertAction actionWithTitle:@"我知道了"
															   style:UIAlertActionStyleDefault
															 handler:^(UIAlertAction * _Nonnull action) {
															 }];
			[alert addAction:confirm];
			[self.window.rootViewController presentViewController:alert animated:YES completion:nil];
		});
	}
}


@end
