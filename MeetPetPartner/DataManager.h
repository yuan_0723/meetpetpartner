//
//  DataManager.h
//  MeetPetPartner
//
//  Created by Yvonne.H on 2017/2/16.
//  Copyright © 2017年 YvonneH. All rights reserved.
//

#import <Foundation/Foundation.h>

/// APP的原始網址
#define APP_SHARE_URL		@"https://itunes.apple.com/tw/app/id1212078175"

// 農委會開放資料json網址
#define ADOPTION_INFO_URL @"http://data.coa.gov.tw/Service/OpenData/TransService.aspx?UnitId=QcbUEzN6E6DL"

// 浪浪的所有毛色
#define PET_CLOUR_ARRAY @[@"不限", @"米白色", @"橘黃色", @"棕色", @"黑色", @"虎斑", @"花色"]

// 浪浪的所有居住地
#define PET_LOCATION_ARRAY @[@"不限", @"基隆", @"臺北", @"新北", @"桃園", @"新竹", @"苗栗", @"臺中", @"南投", @"彰化", @"雲林", @"嘉義", @"臺南", @"高雄", @"屏東", @"臺東", @"花蓮", @"宜蘭", @"連江", @"澎湖", @"金門"]

// json政府資料預設的key值
#define PET_ID					@"animal_id"
#define PET_PHOTO_URL			@"album_file"
#define PET_KIND				@"animal_kind"
#define PET_GENDER				@"animal_sex"
#define PET_BODY_SIZE			@"animal_bodytype"
#define PET_COLOUR				@"animal_colour"
#define PET_AGE					@"animal_age"
#define PET_STERILIZATION		@"animal_sterilization"
#define PET_BACTERIN			@"animal_bacterin"
#define PET_REMARK				@"animal_remark"
#define PET_CAPTION				@"animal_caption"
#define PET_OPENDATE			@"animal_opendate"
#define PET_UPDATE				@"animal_update"
#define PET_CREATETIME			@"animal_createtime"
#define PET_SHELTER				@"shelter_name"
#define PET_SHELTER_TEL			@"shelter_tel"
#define PET_SHELTER_ADDRESS		@"shelter_address"

// json政府資料預設的value值
#define PET_GENDER_INFO			@[@"F", @"M"]
#define PET_BODYTYPE_INFO		@[@"MINI", @"SMALL", @"MEDIUM" , @"BIG"]
#define PET_AGE_INFO			@[@"CHILD", @"ADULT"]
#define PET_KIND_INFO			@[@"貓", @"狗"]
#define PET_STERILIZATION_INFO	@[@"T", @"Y", @"N", @"F"]
#define PET_BACTERIN_INFO		@[@"T", @"Y", @"N", @"F"]

// 顯示給使用者的領養資訊文字
#define SHOW_USER_GENDER		@[@"格格", @"阿哥"]
#define SHOW_USER_BODYTYPE		@[@"小型", @"中型", @"大型"]
#define SHOW_USER_AGE			@[@"幼年", @"成年"]
#define SHOW_USER_KIND			@[@"喵喵貓", @"汪汪狗"]
#define SHOW_USER_STERILIZATION	@[@"Yes", @"No"]
#define SHOW_USER_BACTERIN		@[@"Yes", @"No"]
#define SHOW_UNKNOWN_INFO		@"待確認..."

// UserDefaults設定的key值
#define USER_SETTING_FAVORITE			@"userFavoriteAdoption"
#define USER_SETTING_INDEXPAGESHOW		@"indexPageShow"

@interface DataManager : NSObject

/// 浪浪領養清單(全)
@property NSMutableArray *adoptPetsList;

/// 使用者蒐藏的浪浪編號
@property NSMutableArray *userFavoriteAdoption;

/// 首頁顯示的設定值
@property NSNumber *indexPageShow;


/// 創造singleton物件的方法
+ (instancetype)sharedInstance;


/// 設定應用初始值的方法
- (void)defaultSetting;

/// 將資料從userdefault中取出到單例的方法
- (void)loadSetting;

/// 將資料寫進userdefault裡的方法
- (void)writeSetting:(id)object forKey:(NSString *)defaultKey;



/// 從政府開放資料下載回浪浪領養清單的方法
- (void)getAdoptPetsList:(void (^)())done;
/// 執行urlscheme的方法
+ (void)launchURLSchemeAction:(NSString*) stringURLScheme;


/// 判斷網路狀態是否正常的方法
- (BOOL)isNetworkStatusOK;


/// 將原始json字串轉換給使用者觀看的文字訊息的方法
+ (NSString*)showUserPetKindInfo:(NSString*)petKind;
+ (NSString*)showUserPetGender:(NSString*)petGender;
+ (NSString*)showUserBodyTypeInfo:(NSString*)bodyType;
+ (NSString*)showUserAgeInfo:(NSString*)ageInfo;
+ (NSString*)showUserSterilizationInfo:(NSString*)sterilizationInfo;
+ (NSString*)showUserBacterinInfo:(NSString*)bacterinInfo;


/// 搭配SDWebImage的清除快取機制
- (void)deleteSDWebImageCache;

@end
