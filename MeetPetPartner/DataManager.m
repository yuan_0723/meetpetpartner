//
//  DataManager.m
//  MeetPetPartner
//
//  Created by Yvonne.H on 2017/2/16.
//  Copyright © 2017年 YvonneH. All rights reserved.
//

#import "DataManager.h"
#import "Reachability.h"
#import <SDWebImageManager.h>

@implementation DataManager

#pragma mark - Singleton Data Methods

static DataManager *_singletonDataManager = nil;
static NSNotificationCenter *center = nil;

/// 創造singleton物件的方法
+(instancetype) sharedInstance {
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		
		_singletonDataManager = [[super allocWithZone:NULL] init];
		NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
		
		// 從UserDefaults中讀取資料到單例物件
		if ([userDefaults objectForKey:@"defaultSetting"] == nil) {
			
			// 初次使用app載入應用設定預設值
			[_singletonDataManager defaultSetting];
			[userDefaults setObject:@"beenHere" forKey:@"defaultSetting"];
			
		} else {
			// 不是第一次進入應用程式則載入原先的設定值
			[_singletonDataManager loadSetting];
		}
		
	});
	return _singletonDataManager;
}

/// 設定應用初始值的方法
- (void)defaultSetting {
	NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
	// 初始化浪浪收藏清單
	_userFavoriteAdoption = [NSMutableArray new];
	[userDefaults setObject:_userFavoriteAdoption forKey:USER_SETTING_FAVORITE];
	// 初始化首頁顯示設定
	_indexPageShow = @2;
	[userDefaults setObject:_indexPageShow forKey:USER_SETTING_INDEXPAGESHOW];
}

/// 將資料從userdefault中取出到單例的方法
- (void)loadSetting {
	NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
	
	_userFavoriteAdoption = [[userDefaults objectForKey:USER_SETTING_FAVORITE] mutableCopy];
	_indexPageShow = [userDefaults objectForKey:USER_SETTING_INDEXPAGESHOW];
}


/// 將資料寫進userdefault裡的方法
- (void)writeSetting:(id)object forKey:(NSString *)defaultKey {
	
	// 加入監聽通知，每次userdefault值被修改時與singleton物件同步
	if (center == nil) {
		center = [NSNotificationCenter defaultCenter];
		[center addObserver:self selector:@selector(loadSetting) name:NSUserDefaultsDidChangeNotification object:nil];
	}
	
	// 將資料物件寫進userdefault
	NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
	[userDefaults setObject:object forKey:defaultKey];
	
	[userDefaults synchronize];
}


#pragma mark - Network Transmission Methods

/// 從政府開放資料下載回浪浪領養清單的方法
- (void)getAdoptPetsList:(void (^)())done {
	
	NSURL * downloadURL = [NSURL URLWithString:ADOPTION_INFO_URL];
	
	//針對網路資料傳輸做設定
	NSURLSessionConfiguration * downloadConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
	NSURLSession * downloadSession = [NSURLSession sessionWithConfiguration:downloadConfig];
	
	NSURLSessionDataTask * downloadTask = [downloadSession dataTaskWithURL:downloadURL completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
		
		//失敗的話，則return
		if (error) {
			NSLog(@" - (void)getAdoptPetsList Method Error: %@",error);
			return;
		}
		
		//解析json
		//將資料轉換為NSMutbleDictionary
		NSLog(@"即將開始解析下載資料");
		NSMutableArray *tmpDownloadData = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
		
		for (NSMutableDictionary *tmpPetInfo in tmpDownloadData) {
			
			// 移除不必要的資訊
			tmpPetInfo[@"cDate"] = nil;
			tmpPetInfo[@"album_update"] = nil;
			tmpPetInfo[@"album_base64"] = nil;
			tmpPetInfo[@"album_name"] = nil;
			tmpPetInfo[@"animal_closeddate"] = nil;
			tmpPetInfo[@"animal_title"] = nil;
			tmpPetInfo[@"animal_status"] = nil;
			tmpPetInfo[@"animal_foundplace"] = nil;
			tmpPetInfo[@"animal_place"] = nil;
			tmpPetInfo[@"animal_shelter_pkid"] = nil;
			tmpPetInfo[@"animal_area_pkid"] = nil;
			tmpPetInfo[@"animal_subid"] = nil;
		}
		
		_adoptPetsList = [[NSMutableArray alloc]init];
		_adoptPetsList = tmpDownloadData;
		
		// 確認好下載玩資料後則執行閉包方法
		done();
	}];
	[downloadTask resume];
}

/// 執行urlscheme的方法
+ (void)launchURLSchemeAction:(NSString*) stringURLScheme {
  
  [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[stringURLScheme stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]] options:@{} completionHandler:nil];
}

#pragma mark - Reachability Methods

/// 判斷網路狀態是否正常的方法
- (BOOL)isNetworkStatusOK {
	
	NetworkStatus nowNetworkstatus = [[Reachability reachabilityForInternetConnection] currentReachabilityStatus];

	BOOL isReachableViaWWAN;
	BOOL isReachableViaWifi;
	BOOL isReachable;

	if (nowNetworkstatus == ReachableViaWWAN) {
		isReachableViaWWAN = true;
	} else {
		isReachableViaWWAN = false;
	}

	if (nowNetworkstatus == ReachableViaWiFi) {
		isReachableViaWifi = true;
	} else {
		isReachableViaWifi = false;
	}

	isReachable = (isReachableViaWifi || isReachableViaWWAN);
	return isReachable;
}


#pragma mark - ShowUserInfo Methods

/// 轉換寵物類型的json內容顯示給使用者
+ (NSString*)showUserPetKindInfo:(NSString*)petKind {
	
	NSString *showUserPetKind;
	
	if ([petKind isEqualToString:PET_KIND_INFO[0]]) {
		showUserPetKind = SHOW_USER_KIND[0];
	} else if ([petKind isEqualToString:PET_KIND_INFO[1]]) {
		showUserPetKind = SHOW_USER_KIND[1];
	} else {
		showUserPetKind = SHOW_UNKNOWN_INFO;
	}
	return showUserPetKind;
}

/// 轉換寵物性別的json內容顯示給使用者
+ (NSString*)showUserPetGender:(NSString*)petGender {
	
	NSString *showUserPetGender;
	
	if ([petGender isEqualToString:PET_GENDER_INFO[0]]) {
		showUserPetGender = SHOW_USER_GENDER[0];
	} else if ([petGender isEqualToString:PET_GENDER_INFO[1]]) {
		showUserPetGender = SHOW_USER_GENDER[1];
	} else {
		showUserPetGender = SHOW_UNKNOWN_INFO;
	}
	return showUserPetGender;
}

/// 轉換寵物體型的json內容顯示給使用者
+ (NSString*)showUserBodyTypeInfo:(NSString*)bodyType {
	
	NSString *showUserBodyType;

	if ([bodyType isEqualToString:PET_BODYTYPE_INFO[0]] || [bodyType isEqualToString:PET_BODYTYPE_INFO[1]]) {
		showUserBodyType = SHOW_USER_BODYTYPE[0];
	} else if ([bodyType isEqualToString:PET_BODYTYPE_INFO[2]]) {
		showUserBodyType = SHOW_USER_BODYTYPE[1];
	} else if ([bodyType isEqualToString:PET_BODYTYPE_INFO[3]]) {
		showUserBodyType = SHOW_USER_BODYTYPE[2];
	} else {
		showUserBodyType = SHOW_UNKNOWN_INFO;
	}
	return showUserBodyType;
}

/// 轉換寵物年齡的json內容顯示給使用者
+ (NSString*)showUserAgeInfo:(NSString*)ageInfo {
	
	NSString *showUserAgeInfo;
	
	if ([ageInfo isEqualToString:PET_AGE_INFO[0]]) {
		showUserAgeInfo = SHOW_USER_AGE[0];
	} else if ([ageInfo isEqualToString:PET_AGE_INFO[1]]) {
		showUserAgeInfo = SHOW_USER_AGE[1];
	} else {
		showUserAgeInfo = SHOW_UNKNOWN_INFO;
	}
	return showUserAgeInfo;
}

/// 轉換寵物是否節育的json內容顯示給使用者
+ (NSString*)showUserSterilizationInfo:(NSString*)sterilizationInfo {
	
	NSString *showUserSterilizationInfo;
	
	if ([sterilizationInfo isEqualToString:PET_STERILIZATION_INFO[0]] || [sterilizationInfo isEqualToString:PET_STERILIZATION_INFO[1]]) {
		showUserSterilizationInfo = SHOW_USER_STERILIZATION[0];
	} else if ([sterilizationInfo isEqualToString:PET_STERILIZATION_INFO[2]] || [sterilizationInfo isEqualToString:PET_STERILIZATION_INFO[3]]) {
		showUserSterilizationInfo = SHOW_USER_STERILIZATION[1];
	} else {
		showUserSterilizationInfo = SHOW_UNKNOWN_INFO;
	}
	return showUserSterilizationInfo;
}

/// 轉換寵物是否施打疫苗的json內容顯示給使用者
+ (NSString*)showUserBacterinInfo:(NSString*)bacterinInfo {
	
	NSString *showUserBacterinInfo;
	
	if ([bacterinInfo isEqualToString:PET_BACTERIN_INFO[0]] || [bacterinInfo isEqualToString:PET_BACTERIN_INFO[1]]) {
		showUserBacterinInfo = SHOW_USER_BACTERIN[0];
	} else if ([bacterinInfo isEqualToString:PET_BACTERIN_INFO[2]] || [bacterinInfo isEqualToString:PET_BACTERIN_INFO[3]]) {
		showUserBacterinInfo = SHOW_USER_BACTERIN[1];
	} else {
		showUserBacterinInfo = SHOW_UNKNOWN_INFO;
	}
	return showUserBacterinInfo;
}



#pragma mark - SDWebImage Methods
// 配合 SDWebImage 清理緩存圖片
- (void)deleteSDWebImageCache {
	
	SDWebImageManager *manager = [SDWebImageManager sharedManager];
	// 取消正在下載的圖片
	[manager cancelAll];
	// 清除內部緩存
	[manager.imageCache clearMemory];
	// 清除硬碟緩存
	[manager.imageCache cleanDisk];
}

@end
