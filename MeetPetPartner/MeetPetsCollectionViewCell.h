//
//  MeetPetsCollectionViewCell.h
//  MeetPetPartner
//
//  Created by Yvonne.H on 2017/2/16.
//  Copyright © 2017年 YvonneH. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MeetPetsCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *petsGender;
@property (weak, nonatomic) IBOutlet UIImageView *petsImage;
@property (weak, nonatomic) IBOutlet UILabel *petsLocation;

@end
