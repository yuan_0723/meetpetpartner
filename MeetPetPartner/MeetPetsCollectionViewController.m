//
//  MeetPetsCollectionViewController.m
//  MeetPetPartner
//
//  Created by Yvonne.H on 2017/2/16.
//  Copyright © 2017年 YvonneH. All rights reserved.
//
#import "DataManager.h"
#import <SDImageCache.h>
#import <UIImageView+WebCache.h>
#import "MeetPetsCollectionViewController.h"
#import "MeetPetsCollectionViewCell.h"
#import "AdoptionInfoViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface MeetPetsCollectionViewController () <NSURLSessionDataDelegate, NSURLSessionDelegate, NSURLSessionTaskDelegate>
{
	DataManager *dataManager;					// DataManager單例物件
	
	NSMutableArray *adoptPetsList;				// 流浪動物領養清單(全)
	NSMutableArray *showAdoptPetsList;			// 首頁要顯示的流浪動物領養清單
	
	UIView *loadingView;						// ActivityIndicator View
	UIActivityIndicatorView *activityView;		// ActivityIndicator
	UILabel *loadingLabel;						// ActivityIndicator Label
}

@property (weak, nonatomic) IBOutlet UIBarButtonItem *searchBtn;
@property (weak, nonatomic) IBOutlet UIImageView *noInternetImage;

@end

@implementation MeetPetsCollectionViewController

static NSString * const reuseIdentifier = @"MeetPetsCell";

- (void)viewDidLoad {
	[super viewDidLoad];
	
	
	UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
								   initWithTitle:@""
								   style:UIBarButtonItemStylePlain
								   target:nil
								   action:nil];
	self.navigationItem.backBarButtonItem = backButton;
	
	_noInternetImage.hidden = true;
	
	// 創建單例物件
	dataManager = [DataManager sharedInstance];
	showAdoptPetsList = [NSMutableArray new];
	adoptPetsList = [[NSMutableArray alloc]init];
	
	// 資料下載完成前，將tabbar其他功能及搜尋功能鎖上
	[[[[self.tabBarController tabBar]items]objectAtIndex:1]setEnabled:false];
	[[[[self.tabBarController tabBar]items]objectAtIndex:2]setEnabled:false];
	_searchBtn.tintColor = [UIColor clearColor];
	_searchBtn.enabled = false;
	
	// 檢查網路是否正常
	if ([dataManager isNetworkStatusOK]) {
		
		_noInternetImage.hidden = true;
		
		// 切換到main queue顯示下載的ActivityIndicator動畫
		dispatch_async(dispatch_get_main_queue(), ^{
			loadingView = [[UIView alloc] initWithFrame:CGRectMake(75, 155, 170, 170)];
			loadingView.center = CGPointMake(self.view.center.x,loadingView.center.y);
			loadingView.center = CGPointMake(loadingView.center.x, self.view.center.y);
			
			loadingView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
			loadingView.clipsToBounds = YES;
			loadingView.layer.cornerRadius = 10.0;
			
			activityView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
			activityView.frame = CGRectMake(65, 40, activityView.bounds.size.width, activityView.bounds.size.height);
			[loadingView addSubview:activityView];
			
			loadingLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 115, 130, 22)];
			loadingLabel.backgroundColor = [UIColor clearColor];
			loadingLabel.textColor = [UIColor whiteColor];
			loadingLabel.adjustsFontSizeToFitWidth = YES;
			loadingLabel.textAlignment = NSTextAlignmentCenter;
			loadingLabel.text = @"資料下載中，請稍後...";
			[loadingView addSubview:loadingLabel];
			
			[self.view addSubview:loadingView];
			[activityView startAnimating];
		});
		
		// 從伺服器取得領養資料
		[dataManager getAdoptPetsList:^{
			
			adoptPetsList = dataManager.adoptPetsList;
			
			// 根據使用者的首頁設定，篩選要呈現的內容
			if ([dataManager.indexPageShow isEqual:@0]) {
				
				for (NSDictionary *tmpAdoptInfo in adoptPetsList) {
					if ([tmpAdoptInfo[PET_KIND] isEqualToString:PET_KIND_INFO[0]]) {
						[showAdoptPetsList addObject:tmpAdoptInfo];
					}
				}
				
			} else if ([dataManager.indexPageShow isEqual:@1]) {
				
				for (NSDictionary *tmpAdoptInfo in adoptPetsList) {
					if ([tmpAdoptInfo[PET_KIND] isEqualToString:PET_KIND_INFO[1]]) {
						[showAdoptPetsList addObject:tmpAdoptInfo];
					}
				}
				
			} else {
				showAdoptPetsList = dataManager.adoptPetsList;
			}
	
			
			// 確保以上的事情執行完才執行以下程式碼
			dispatch_async(dispatch_get_main_queue(), ^{
				
				self.collectionView.delegate = self;
				self.collectionView.dataSource = self;
				
				[self.collectionView reloadData];
				
				[activityView stopAnimating];
				[loadingView removeFromSuperview];
				
				[[[[self.tabBarController tabBar]items]objectAtIndex:1]setEnabled:true];
				[[[[self.tabBarController tabBar]items]objectAtIndex:2]setEnabled:true];
				
				_searchBtn.tintColor = [UIColor whiteColor];
				_searchBtn.enabled = true;
				
			});
		}];
		
	} else {
		dispatch_async(dispatch_get_main_queue(), ^{
			
			_noInternetImage.hidden = false;
			
			// 如果手機沒有連到網路的話，顯示警告視窗
			UIAlertController * alert = [UIAlertController alertControllerWithTitle:@"無法連結網路"
																		message:@"請開啟您的網路並重新開啟APP連接政府資料庫"
																 preferredStyle:UIAlertControllerStyleAlert];
		
			UIAlertAction * cancel = [UIAlertAction actionWithTitle:@"知道了"
														   style:UIAlertActionStyleCancel
														 handler:^(UIAlertAction * _Nonnull action) {}];
      
			[alert addAction:cancel];
			[self presentViewController:alert animated:YES completion:nil];
		});
	}
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
	// 配合 SDWebImage 清理緩存圖片
	[dataManager deleteSDWebImageCache];
}

#pragma mark - UICollectionViewDataSource Methods

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return showAdoptPetsList.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
	
	MeetPetsCollectionViewCell *meetPetsCell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
	
	// 設置cell的外觀
	meetPetsCell.layer.masksToBounds = NO;
	meetPetsCell.layer.borderColor = [UIColor grayColor].CGColor;
	meetPetsCell.layer.contentsScale = [UIScreen mainScreen].scale;
	meetPetsCell.layer.shadowOpacity = 0.4f;
	meetPetsCell.layer.shadowRadius = 1.0f;
	meetPetsCell.layer.shadowOffset = CGSizeZero;
	meetPetsCell.layer.shadowPath = [UIBezierPath bezierPathWithRect:meetPetsCell.bounds].CGPath;
	
	// 流浪動物居住地
	NSString *petLocation = [showAdoptPetsList[indexPath.row][PET_SHELTER] substringToIndex:3];
	if ([petLocation isEqualToString:@"苗栗動"]) {
		petLocation = @"苗栗縣";		// 校正苗栗縣
	}
	meetPetsCell.petsLocation.text = petLocation;
	
	// 設定照片顯示
	if ([showAdoptPetsList[indexPath.row][PET_PHOTO_URL] isEqualToString:@""]) {
		
		if ([showAdoptPetsList[indexPath.row][PET_KIND] isEqualToString:PET_KIND_INFO[0]]) {
			meetPetsCell.petsImage.image = [UIImage imageNamed:@"CatNoImage"];
		} else {
			meetPetsCell.petsImage.image = [UIImage imageNamed:@"DogNoImage"];
		}
		
	} else {
		
		NSURL *imgURL = [NSURL URLWithString:showAdoptPetsList[indexPath.row][PET_PHOTO_URL]];
		[meetPetsCell.petsImage sd_setImageWithURL:imgURL placeholderImage:[UIImage imageNamed:@"ImageLoading"]];
		
	}
	
	
	// 設定種類與性別圖示
	if ([showAdoptPetsList[indexPath.row][PET_KIND] isEqualToString:PET_KIND_INFO[0]]) {
		
		if ([showAdoptPetsList[indexPath.row][PET_GENDER] isEqualToString:PET_GENDER_INFO[0]]) {
			meetPetsCell.petsGender.image = [UIImage imageNamed:@"catGirl"];
		} else if ([showAdoptPetsList[indexPath.row][PET_GENDER] isEqualToString:PET_GENDER_INFO[1]]) {
			meetPetsCell.petsGender.image = [UIImage imageNamed:@"catBoy"];
		} else {
			meetPetsCell.petsGender.image = [UIImage imageNamed:@"catUnknown"];
		}
		
	} else if ([showAdoptPetsList[indexPath.row][PET_KIND] isEqualToString:PET_KIND_INFO[1]]) {
		
		if ([showAdoptPetsList[indexPath.row][PET_GENDER] isEqualToString:PET_GENDER_INFO[0]]) {
			meetPetsCell.petsGender.image = [UIImage imageNamed:@"dogGirl"];
		} else if ([showAdoptPetsList[indexPath.row][PET_GENDER] isEqualToString:PET_GENDER_INFO[1]]) {
			meetPetsCell.petsGender.image = [UIImage imageNamed:@"dogBoy"];
		} else {
			meetPetsCell.petsGender.image = [UIImage imageNamed:@"dogUnknown"];
		}
		
	}
	return meetPetsCell;
}


-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
	
	// 點擊cell後跳轉至領養資訊頁
	AdoptionInfoViewController *adoptionInfoPage = [self.storyboard instantiateViewControllerWithIdentifier:@"AdoptionInfoPage"];
	
	adoptionInfoPage.adoptionInfo = showAdoptPetsList[indexPath.row];
	
	[self showViewController:adoptionInfoPage sender:self];
	
}

#pragma mark - UICollectionViewDelegate Methods

// 設定cell的尺寸
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
	float cellWidth = (self.view.frame.size.width / 2.0f) - 17.5f;
	float cellHeight = cellWidth/0.80952381f;
	return CGSizeMake(cellWidth, cellHeight);
}


@end
