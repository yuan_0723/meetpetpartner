//
//  MyFavoriteCollectionViewCell.h
//  MeetPetPartner
//
//  Created by Yvonne.H on 2017/3/3.
//  Copyright © 2017年 YvonneH. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyFavoriteCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *petPhoto;
@property (weak, nonatomic) IBOutlet UIImageView *kindGenderImage;
@property (weak, nonatomic) IBOutlet UILabel *bodyTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *ageLabel;
@property (weak, nonatomic) IBOutlet UILabel *sterilizationLabel;
@property (weak, nonatomic) IBOutlet UILabel *baceterinLabel;
@property (weak, nonatomic) IBOutlet UILabel *shelterNameLabel;
@property (weak, nonatomic) IBOutlet UIButton *deleteFavoriteBtn;
@property (weak, nonatomic) IBOutlet UIButton *addFavoriteBtn;


@end
