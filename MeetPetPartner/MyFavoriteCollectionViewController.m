//
//  MyFavoriteCollectionViewController.m
//  MeetPetPartner
//
//  Created by Yvonne.H on 2017/3/3.
//  Copyright © 2017年 YvonneH. All rights reserved.
//

#import "MyFavoriteCollectionViewController.h"
#import "MyFavoriteCollectionViewCell.h"
#import "DataManager.h"
#import <UIImageView+WebCache.h>
#import "AdoptionInfoViewController.h"

@interface MyFavoriteCollectionViewController () {
	DataManager *dataManager;
	NSMutableArray *favoritePetIDs;
	NSMutableArray *allPetsAdoption;
	NSMutableArray *favoritePets;
	MyFavoriteCollectionViewCell *myFavoriteCell;
	UIView *loadingView;
	UILabel *loadingLabel;
}
@property (weak, nonatomic) IBOutlet UIImageView *noFavoriteImage;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *deleteAllFavoriteBtn;

@end

@implementation MyFavoriteCollectionViewController

static NSString * const reuseIdentifier = @"MyFavoriteCell";

- (void)viewDidLoad {
	[super viewDidLoad];
	_noFavoriteImage.hidden = true;
	
	UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
								   initWithTitle:@""
								   style:UIBarButtonItemStylePlain
								   target:nil
								   action:nil];
	self.navigationItem.backBarButtonItem = backButton;
	
	// 創造單例物件
	dataManager = [DataManager sharedInstance];
	[dataManager loadSetting];
	// 從單例物件讀取所有認養資料
	allPetsAdoption = [NSMutableArray new];
	allPetsAdoption = dataManager.adoptPetsList;
	favoritePetIDs = [NSMutableArray new];
	favoritePets = [NSMutableArray new];
	
	_deleteAllFavoriteBtn.tintColor = [UIColor clearColor];
	_deleteAllFavoriteBtn.enabled = false;
}

-(void) viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
	
	// 清空所有暫存的我的最愛寵物資料
	[favoritePets removeAllObjects];
	favoritePetIDs = dataManager.userFavoriteAdoption;
	
	// 過濾符合我的最愛寵物編號的字典物件並存放於favoritePets陣列內
	for (NSDictionary *tmpAdoptionInfo in allPetsAdoption) {
		if ([favoritePetIDs containsObject:tmpAdoptionInfo[PET_ID]]) {
			[favoritePets addObject:tmpAdoptionInfo];
		}
	}
	
	// 重新整理collectionView
	[self.collectionView reloadData];
	
	// 若沒有我的最愛則顯示指引圖片
	if (favoritePets.count == 0) {
		_noFavoriteImage.hidden = false;
		_deleteAllFavoriteBtn.tintColor = [UIColor clearColor];
		_deleteAllFavoriteBtn.enabled = false;
	} else {
		_noFavoriteImage.hidden = true;
		_deleteAllFavoriteBtn.tintColor = [UIColor whiteColor];
		_deleteAllFavoriteBtn.enabled = true;
	}
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
	[dataManager deleteSDWebImageCache];
}

#pragma mark - IBAction Methods
/// 刪除我的最愛的方法
- (IBAction)deleteFavoriteBtnPressed:(UIButton *)sender {
	// 取得indexpath
	NSIndexPath *indexPath = nil;
	indexPath = [self.collectionView indexPathForItemAtPoint:[self.collectionView convertPoint:sender.center fromView:sender.superview]];
	[favoritePetIDs removeObject:favoritePets[indexPath.row][PET_ID]];
	// 將變動的資料寫進userdefault
	[dataManager writeSetting:favoritePetIDs forKey:USER_SETTING_FAVORITE];
	
	// 切換到main queue做介面更動
	dispatch_async(dispatch_get_main_queue(), ^{
		myFavoriteCell.addFavoriteBtn.hidden = false;
		myFavoriteCell.deleteFavoriteBtn.hidden = true;
		[self.collectionView reloadData];
	});

}

/// 加入我的最愛的方法
- (IBAction)addFavoriteBtnPressed:(UIButton *)sender {
	// 取得indexpath
	NSIndexPath *indexPath = nil;
	indexPath = [self.collectionView indexPathForItemAtPoint:[self.collectionView convertPoint:sender.center fromView:sender.superview]];
	[favoritePetIDs addObject:favoritePets[indexPath.row][PET_ID]];
	// 將變動的資料寫進userdefault
	[dataManager writeSetting:favoritePetIDs forKey:USER_SETTING_FAVORITE];
	// 切換到main queue做介面更動
	dispatch_async(dispatch_get_main_queue(), ^{
		myFavoriteCell.addFavoriteBtn.hidden = true;
		myFavoriteCell.deleteFavoriteBtn.hidden = false;
		[self.collectionView reloadData];
	});
}

/// 清空所有我的最愛的方法
- (IBAction)deleteAllFavorite:(UIBarButtonItem *)sender {
	//準備警告視窗
	UIAlertController * alert = [UIAlertController alertControllerWithTitle:@"注意！"
										message:@"確定要將最愛的浪浪資料全部刪除嗎？或是您可以選擇點擊愛心圖示個別刪除最愛的浪浪。"
								 preferredStyle:UIAlertControllerStyleAlert];
	
	//準備警告視窗上的取消按鈕
	UIAlertAction * cancel = [UIAlertAction actionWithTitle:@"取消"
													  style:UIAlertActionStyleCancel
													handler:^(UIAlertAction * _Nonnull action) {}];
	
	//準備警告視窗上的確認按鈕
	UIAlertAction * confirm = [UIAlertAction actionWithTitle:@"全部刪除"
													   style:UIAlertActionStyleDefault
													 handler:^(UIAlertAction * _Nonnull action) {
														 
														 [favoritePetIDs removeAllObjects];
														 [dataManager writeSetting:favoritePetIDs forKey:USER_SETTING_FAVORITE];
														 
														 dispatch_async(dispatch_get_main_queue(), ^{
															 [self viewWillAppear:true];
														 });
													 }];
	//將按鈕加到警告視窗上
	[alert addAction:cancel];
	[alert addAction:confirm];
	//顯示警告視窗
	[self presentViewController:alert animated:YES completion:nil];
	
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return favoritePets.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    myFavoriteCell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
	
	// 設置cell的外觀
	myFavoriteCell.layer.masksToBounds = NO;
	myFavoriteCell.layer.borderColor = [UIColor grayColor].CGColor;
	myFavoriteCell.layer.contentsScale = [UIScreen mainScreen].scale;
	myFavoriteCell.layer.shadowOpacity = 0.4f;
	myFavoriteCell.layer.shadowRadius = 1.0f;
	myFavoriteCell.layer.shadowOffset = CGSizeZero;
	myFavoriteCell.layer.shadowPath = [UIBezierPath bezierPathWithRect:myFavoriteCell.bounds].CGPath;
	
	// 要顯示在cell上元件設定
	// 設定流浪所資訊
	myFavoriteCell.shelterNameLabel.text = favoritePets[indexPath.row][PET_SHELTER];
	
	// 設定體型顯示
	myFavoriteCell.bodyTypeLabel.text = [DataManager showUserBodyTypeInfo:favoritePets[indexPath.row][PET_BODY_SIZE]];
	
	// 設定年紀顯示
	myFavoriteCell.ageLabel.text = [DataManager showUserAgeInfo:favoritePets[indexPath.row][PET_AGE]];
	
	// 設定絕育顯示
	myFavoriteCell.sterilizationLabel.text = [DataManager showUserSterilizationInfo:favoritePets[indexPath.row][PET_STERILIZATION]];
	
	// 設定疫苗施打顯示
	myFavoriteCell.baceterinLabel.text = [DataManager showUserBacterinInfo:favoritePets[indexPath.row][PET_BACTERIN]];
	
	// 設定收藏按鈕是否隱藏
	if ([favoritePetIDs containsObject:favoritePets[indexPath.row][PET_ID]]) {
		myFavoriteCell.addFavoriteBtn.hidden =true;
		myFavoriteCell.deleteFavoriteBtn.hidden = false;
	} else {
		myFavoriteCell.addFavoriteBtn.hidden =false;
		myFavoriteCell.deleteFavoriteBtn.hidden = true;
	}
	
		// 設定照片顯示
	if ([favoritePets[indexPath.row][PET_PHOTO_URL] isEqualToString:@""]) {
		if ([favoritePets[indexPath.row][PET_KIND] isEqualToString:PET_KIND_INFO[0]]) {
			myFavoriteCell.petPhoto.image = [UIImage imageNamed:@"CatNoImage"];
		} else {
			myFavoriteCell.petPhoto.image = [UIImage imageNamed:@"DogNoImage"];
		}
	} else {
		NSURL *imgURL = [NSURL URLWithString:favoritePets[indexPath.row][PET_PHOTO_URL]];
		[myFavoriteCell.petPhoto sd_setImageWithURL:imgURL placeholderImage:[UIImage imageNamed:@"ImageLoading"]];
	}
	
	
	// 設定種類與性別顯示
	if ([favoritePets[indexPath.row][PET_KIND] isEqualToString:PET_KIND_INFO[0]]) {
		
		if ([favoritePets[indexPath.row][PET_GENDER] isEqualToString:PET_GENDER_INFO[0]]) {
			myFavoriteCell.kindGenderImage.image = [UIImage imageNamed:@"catGirl"];
		} else if ([favoritePets[indexPath.row][PET_GENDER] isEqualToString:PET_GENDER_INFO[1]]) {
			myFavoriteCell.kindGenderImage.image = [UIImage imageNamed:@"catBoy"];
		} else {
			myFavoriteCell.kindGenderImage.image = [UIImage imageNamed:@"catUnknown"];
		}
		
	} else if ([favoritePets[indexPath.row][PET_KIND] isEqualToString:PET_KIND_INFO[1]]) {
		
		if ([favoritePets[indexPath.row][PET_GENDER] isEqualToString:PET_GENDER_INFO[0]]) {
			myFavoriteCell.kindGenderImage.image = [UIImage imageNamed:@"dogGirl"];
		} else if ([favoritePets[indexPath.row][PET_GENDER] isEqualToString:PET_GENDER_INFO[1]]) {
			myFavoriteCell.kindGenderImage.image = [UIImage imageNamed:@"dogBoy"];
		} else {
			myFavoriteCell.kindGenderImage.image = [UIImage imageNamed:@"dogUnknown"];
		}
		
	}
    return myFavoriteCell;
}


#pragma mark UICollectionViewDelegate Method

// 點擊cell執行頁面跳轉
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
	
	AdoptionInfoViewController *adoptionInfoPage = [self.storyboard instantiateViewControllerWithIdentifier:@"AdoptionInfoPage"];
	
	adoptionInfoPage.adoptionInfo = favoritePets[indexPath.row];
	
	[self showViewController:adoptionInfoPage sender:self];
	
}

// 設定cell的尺寸
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
	float cellWidth = ((self.view.frame.size.width) / 375.0f) * 350.0f;
	float cellHeight = ((self.view.frame.size.width) / 375.0f) * 185.0f;
	return CGSizeMake(cellWidth, cellHeight);
}

@end
