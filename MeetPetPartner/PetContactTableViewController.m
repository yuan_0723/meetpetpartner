//
//  PetContactTableViewController.m
//  MeetPetPartner
//
//  Created by Yvonne.H on 2017/2/20.
//  Copyright © 2017年 YvonneH. All rights reserved.
//

#import <MapKit/MapKit.h>
#import "DataManager.h"
#import "PetContactTableViewController.h"
#import "ShelterMapViewController.h"

#define GOOGLE_SEARCH_URL		@"http://www.google.com/search?q="
#define TELEPHONE_CALL_URL		@"tel:"

@interface PetContactTableViewController ()

{
	NSString *shelterAddressStr;
	MKPlacemark *placemark;
}

@property (weak, nonatomic) IBOutlet UILabel *shelterName;
@property (weak, nonatomic) IBOutlet UILabel *shelterPhone;
@property (weak, nonatomic) IBOutlet UILabel *shelterAddress;
@property (weak, nonatomic) IBOutlet MKMapView *shelterMap;

@end

@implementation PetContactTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
	
	// 切換到ui tread 顯示地圖
	dispatch_async(dispatch_get_main_queue(), ^{
		CLGeocoder *geocoder = [[CLGeocoder alloc] init];
		[geocoder geocodeAddressString:shelterAddressStr
					 completionHandler:^(NSArray* placemarks, NSError* error){
						 
						 if (placemarks && placemarks.count > 0) {
							 
							 CLPlacemark *topResult = [placemarks objectAtIndex:0];
							 placemark = [[MKPlacemark alloc] initWithPlacemark:topResult];
							 
							 MKCoordinateRegion region = _shelterMap.region;
							 
							 region.center = [(CLCircularRegion *)placemark.region center];
							 region.span.longitudeDelta /= 300.0;
							 region.span.latitudeDelta /= 300.0;
							 
							 [_shelterMap setRegion:region animated:false];
							 [_shelterMap addAnnotation:placemark];
						 }
						 
					 }
		 ];
	});
}

-(void)viewWillAppear:(BOOL)animated {
	
	[super viewWillAppear:animated];
	
	shelterAddressStr = _adoptionInfo[PET_SHELTER_ADDRESS];
	
	if ([_adoptionInfo[PET_SHELTER] isEqualToString:@""]) {
		_shelterName.text = SHOW_UNKNOWN_INFO;
	} else {
		_shelterName.text = _adoptionInfo[PET_SHELTER];
	}
	
	if ([_adoptionInfo[PET_SHELTER_ADDRESS] isEqualToString:@""]) {
		_shelterAddress.text = SHOW_UNKNOWN_INFO;
	} else {
		_shelterAddress.text = _adoptionInfo[PET_SHELTER_ADDRESS];
	}
	
	// 收容所電話號碼校正
	if ([_adoptionInfo[PET_SHELTER_TEL] isEqualToString:@""]) {
		_shelterPhone.text = SHOW_UNKNOWN_INFO;
	} else if ([_adoptionInfo[PET_SHELTER] isEqualToString:@"臺北市動物之家"]) {
		_shelterPhone.text = @"02-87913254";
	} else if ([_adoptionInfo[PET_SHELTER] isEqualToString:@"桃園市動物保護教育園區"]) {
		_shelterPhone.text = @"03-4861760";
	} else if ([_adoptionInfo[PET_SHELTER] isEqualToString:@"嘉義縣流浪犬中途之家"]) {
		_shelterPhone.text = @"05-2724721";
	} else if ([_adoptionInfo[PET_SHELTER] isEqualToString:@"屏東縣流浪動物收容所"]) {
		_shelterPhone.text = @"08-7701094";
	} else {
		_shelterPhone.text = _adoptionInfo[PET_SHELTER_TEL];
	}
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	
	// 搜尋浪浪住所的相關資訊
	if (indexPath.section==0 && indexPath.row==0) {
		if (![_adoptionInfo[PET_SHELTER]isEqualToString:@""]) {
			NSString *googleShelterURL = [NSString stringWithFormat:@"%@%@", GOOGLE_SEARCH_URL, _adoptionInfo[PET_SHELTER]];
			[DataManager launchURLSchemeAction:googleShelterURL];
		}
	}
	
	// 播打電話給浪浪住所
	if (indexPath.section==0 && indexPath.row==1) {
		
		if (![_adoptionInfo[PET_SHELTER_TEL] isEqualToString:@""] && ![_adoptionInfo[PET_SHELTER] isEqualToString:@"雲林縣流浪動物收容所"]) {
			
			dispatch_async(dispatch_get_main_queue(), ^{
				
				NSString *callMessage = [NSString stringWithFormat:@"您確定要播打電話到 %@ (%@) 嗎？", _adoptionInfo[PET_SHELTER], _shelterPhone.text];
				
				UIAlertController * alert = [UIAlertController alertControllerWithTitle:@"要播打電話嗎？"
																				message:callMessage
																		 preferredStyle:UIAlertControllerStyleAlert];

				UIAlertAction * cancel = [UIAlertAction actionWithTitle:@"取消"
																  style:UIAlertActionStyleCancel
																handler:^(UIAlertAction * _Nonnull action) {}];

				UIAlertAction * confirm = [UIAlertAction actionWithTitle:@"撥號"
																   style:UIAlertActionStyleDefault
																 handler:^(UIAlertAction * _Nonnull action) {
																 
																	 NSString *callShelterURL = [NSString stringWithFormat:@"%@%@", TELEPHONE_CALL_URL, _adoptionInfo[PET_SHELTER_TEL]];
																	 [DataManager launchURLSchemeAction:callShelterURL];
																 }];
			
				[alert addAction:cancel];
				[alert addAction:confirm];
				[self presentViewController:alert animated:YES completion:nil];
			});
		}
	}
	
	// 開啟地圖頁
	if ((indexPath.section==1 && indexPath.row==1) || (indexPath.section==1 && indexPath.row==0)) {
		
		if (![_adoptionInfo[PET_SHELTER_ADDRESS]isEqualToString:@""]) {
		
			ShelterMapViewController *shelterMapPage = [self.storyboard instantiateViewControllerWithIdentifier:@"ShelterMapPage"];
			shelterMapPage.adoptionInfo = _adoptionInfo;
			shelterMapPage.shelterPlacemark = placemark;
			[self showViewController:shelterMapPage sender:self];
		}
	}
}

#pragma mark - Table View Delegate & DataSource Methods

/// 調整cell的高度
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	// 如果無地址資料則不顯示地圖介面
	if ((indexPath.section == 1 && indexPath.row == 1) && [_adoptionInfo[PET_SHELTER_ADDRESS] isEqualToString:@""]) {
		return 0;
	}
	
	return [super tableView:tableView heightForRowAtIndexPath:indexPath];
}


@end
