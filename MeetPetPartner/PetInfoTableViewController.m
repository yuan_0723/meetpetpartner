//
//  PetInfoTableViewController.m
//  MeetPetPartner
//
//  Created by Yvonne.H on 2017/2/20.
//  Copyright © 2017年 YvonneH. All rights reserved.
//
#import "DataManager.h"
#import "PetInfoTableViewController.h"

@interface PetInfoTableViewController () {
	DataManager *dataManager;
}

@property (weak, nonatomic) IBOutlet UILabel *petID;
@property (weak, nonatomic) IBOutlet UILabel *petKind;
@property (weak, nonatomic) IBOutlet UILabel *petGender;
@property (weak, nonatomic) IBOutlet UILabel *petSize;
@property (weak, nonatomic) IBOutlet UILabel *petClour;
@property (weak, nonatomic) IBOutlet UILabel *petAge;
@property (weak, nonatomic) IBOutlet UILabel *petSterilization;
@property (weak, nonatomic) IBOutlet UILabel *petBacterin;
@property (weak, nonatomic) IBOutlet UILabel *petNote;
@property (weak, nonatomic) IBOutlet UILabel *petOpenDate;
@property (weak, nonatomic) IBOutlet UILabel *petUpdate;

@end

@implementation PetInfoTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

-(void)viewWillAppear:(BOOL)animated {
	
	[super viewWillAppear:animated];
	
	// 設定顯示資訊
	_petKind.text = [DataManager showUserPetKindInfo:_adoptionInfo[PET_KIND]];
	_petGender.text = [DataManager showUserPetGender:_adoptionInfo[PET_GENDER]];
	_petSize.text = [DataManager showUserBodyTypeInfo:_adoptionInfo[PET_BODY_SIZE]];
	_petAge.text = [DataManager showUserAgeInfo:_adoptionInfo[PET_AGE]];
	_petSterilization.text = [DataManager showUserSterilizationInfo:_adoptionInfo[PET_STERILIZATION]];
	_petBacterin.text = [DataManager showUserBacterinInfo:_adoptionInfo[PET_BACTERIN]];
	
  _petID.text = [NSString stringWithFormat:@"%d", [_adoptionInfo[PET_ID] intValue]];
	_petClour.text = _adoptionInfo[PET_COLOUR];
  _petOpenDate.text = [_adoptionInfo[PET_OPENDATE] stringByReplacingOccurrencesOfString:@"-" withString:@" / "];
  _petUpdate.text = [_adoptionInfo[PET_UPDATE] stringByReplacingOccurrencesOfString:@"/" withString:@" / "];
	
	if (![_adoptionInfo[PET_CAPTION] isEqualToString:@""]) {
		_petNote.text = [NSString stringWithFormat:@"%@ (%@)", _adoptionInfo[PET_REMARK], _adoptionInfo[PET_CAPTION]];
	} else {
		_petNote.text = _adoptionInfo[PET_REMARK];
	}
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
	[dataManager deleteSDWebImageCache];
}

#pragma mark - Table View Delegate & DataSource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	// 介紹的部份依照label的字串數量決定 cell 的高度， 若沒有介紹則隱藏該cell
	if (indexPath.section == 1 && indexPath.row == 2) {
		if ([_petNote.text isEqualToString:@""]) {
			
			return 0;
			
		} else {
			
			CGRect rect = [_petNote.text
						   boundingRectWithSize:CGSizeMake(CGRectGetWidth(self.view.frame)-100, MAXFLOAT)
						   options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading
						   attributes:@{NSFontAttributeName:[UIFont preferredFontForTextStyle:UIFontTextStyleSubheadline]}
						   context:nil];
			
			// +35 為該 cell 內 label + padding 的高度
			return (rect.size.height + 50);
		
		}

	}
	
	return [super tableView:tableView heightForRowAtIndexPath:indexPath];
}

@end
