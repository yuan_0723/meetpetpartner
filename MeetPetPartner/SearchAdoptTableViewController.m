//
//  SearchAdoptTableViewController.m
//  MeetPetPartner
//
//  Created by Yvonne.H on 2017/2/21.
//  Copyright © 2017年 YvonneH. All rights reserved.
//

#import "SearchAdoptTableViewController.h"
#import "SearchResultCollectionViewController.h"
#import "DataManager.h"
#import <QuartzCore/QuartzCore.h>

@interface SearchAdoptTableViewController () <UIPickerViewDelegate, UIPickerViewDataSource>
{
	DataManager *datamanager;
	
	UIActivityIndicatorView *activityView;
	UIView *loadingView;
	UILabel *loadingLabel;
	
	// 所有領養資料陣列
	NSMutableArray *adoptInfoAll;
	
	// 存放搜尋結果的陣列
	NSMutableArray *searchAdoptInfo;
	
	// picker的資料陣列
	NSArray *pickerClour;
	NSArray *pickerLocation;
	
	// 用來搜尋的字串
	NSString *searchGender;
	NSString *searchKind;
	NSString *searchBodyType;
	NSString *searchClour;
	NSString *searchAge;
	NSString *searchLocation;
	
	// pickerView控制
	NSInteger clourPVRow;
	NSInteger locationPVR;
	NSInteger showRowFirstSec;

}

@property (weak, nonatomic) IBOutlet UILabel *clourLabel;
@property (weak, nonatomic) IBOutlet UILabel *locationLabel;
@property (weak, nonatomic) IBOutlet UIPickerView *clourPickerView;
@property (weak, nonatomic) IBOutlet UIPickerView *locationPickerView;
@property (weak, nonatomic) IBOutlet UISegmentedControl *kindSegment;
@property (weak, nonatomic) IBOutlet UISegmentedControl *genderSegment;
@property (weak, nonatomic) IBOutlet UISegmentedControl *bodyTypeSegment;
@property (weak, nonatomic) IBOutlet UISegmentedControl *ageSegment;

@end

@implementation SearchAdoptTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
	
	UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
								   initWithTitle:@""
								   style:UIBarButtonItemStylePlain
								   target:nil
								   action:nil];
	self.navigationItem.backBarButtonItem = backButton;
	
	// 從單例中讀取所有認養資訊
	datamanager = [DataManager sharedInstance];
	adoptInfoAll = datamanager.adoptPetsList;
	
	// 設置搜尋變數的初始值
	_clourLabel.text = PET_CLOUR_ARRAY[0];
	_locationLabel.text = PET_LOCATION_ARRAY[0];
	searchGender = @"";
	searchKind = @"";
	searchBodyType = @"";
	searchAge = @"";
	searchClour = @"";
	searchLocation = @"";
	[_kindSegment setSelectedSegmentIndex:2];
	[_genderSegment setSelectedSegmentIndex:2];
	[_bodyTypeSegment setSelectedSegmentIndex:3];
	[_ageSegment setSelectedSegmentIndex:2];
	
	// 設定picker資料
	pickerClour = PET_CLOUR_ARRAY;
	pickerLocation = PET_LOCATION_ARRAY;
}


-(void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
	
	// 設定picker預設值
	clourPVRow = [pickerClour indexOfObject:_clourLabel.text];
	[_clourPickerView selectRow:clourPVRow inComponent:0 animated:false];
	
	locationPVR = [pickerLocation indexOfObject:_locationLabel.text];
	[_locationPickerView selectRow:locationPVR inComponent:0 animated:false];
	
	// activityView 設定
	dispatch_async(dispatch_get_main_queue(), ^{
		loadingView = [[UIView alloc] initWithFrame:CGRectMake(75, 155, 170, 170)];
		loadingView.center = CGPointMake(self.view.center.x,loadingView.center.y);
		loadingView.center = CGPointMake(loadingView.center.x, self.view.center.y);
		
		loadingView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
		loadingView.clipsToBounds = YES;
		loadingView.layer.cornerRadius = 10.0;
		
		activityView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
		activityView.frame = CGRectMake(65, 40, activityView.bounds.size.width, activityView.bounds.size.height);
		[loadingView addSubview:activityView];
		
		loadingLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 115, 130, 22)];
		loadingLabel.backgroundColor = [UIColor clearColor];
		loadingLabel.textColor = [UIColor whiteColor];
		loadingLabel.adjustsFontSizeToFitWidth = YES;
		loadingLabel.textAlignment = NSTextAlignmentCenter;
		loadingLabel.text = @"搜尋中，請稍後...";
	});

}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	[datamanager deleteSDWebImageCache];
}


#pragma mark - IBAction Methods

/// 按下搜尋按鈕時觸發
- (IBAction)searchBtnPressed:(id)sender {
	
	// 載入圈圈啟動
	[loadingView addSubview:loadingLabel];
	[self.view addSubview:loadingView];
	[activityView startAnimating];
	
	// 跟使用者的操作決定搜尋標準
	[self getSearchInfoBySegment];
	
	searchAdoptInfo = [NSMutableArray new];
	[searchAdoptInfo removeAllObjects];

	// 根據搜尋標準取得搜尋結果
	[self analysisAdoptionForSearch:adoptInfoAll];

	// 停止旋轉動畫
	dispatch_async(dispatch_get_main_queue(), ^{
		[activityView stopAnimating];
		[loadingView removeFromSuperview];
	});

	
	if (searchAdoptInfo.count > 0) {
		
		// 跳轉到搜尋結果頁，並將搜尋結果的字典帶過去
		SearchResultCollectionViewController *searchResultPage = [self.storyboard instantiateViewControllerWithIdentifier:@"SearchResultPage"];
		
		searchResultPage.searchAdoptResult = searchAdoptInfo;
		
		[self showViewController:searchResultPage sender:self];
		
	} else {
		
		// 顯示無搜尋結果
		dispatch_async(dispatch_get_main_queue(), ^{
			UIAlertController * alert = [UIAlertController alertControllerWithTitle:@"沒有相關資料"
																			message:@"沒有搜尋到符合搜尋標準的浪浪資料，建議可以調整搜尋標準後再試試看～"
																	 preferredStyle:UIAlertControllerStyleAlert];
			
			UIAlertAction * cancel = [UIAlertAction actionWithTitle:@"我知道了"
															  style:UIAlertActionStyleCancel
															handler:^(UIAlertAction * _Nonnull action) {}];
			
			[alert addAction:cancel];

			[self presentViewController:alert animated:YES completion:nil];
		});
	}
}

#pragma mark - Private Methods
/// 取得使用者期望的搜尋標準的方法
-(void) getSearchInfoBySegment {
	
	// 取得使用者選取的segment值
	searchGender = [_genderSegment titleForSegmentAtIndex:_genderSegment.selectedSegmentIndex];
	searchKind = [_kindSegment titleForSegmentAtIndex:_kindSegment.selectedSegmentIndex];
	searchAge = [_ageSegment titleForSegmentAtIndex:_genderSegment.selectedSegmentIndex];
	searchBodyType = [_bodyTypeSegment titleForSegmentAtIndex:_bodyTypeSegment.selectedSegmentIndex];
	
	// 根據使用者點及的項目，做搜尋判斷
	
	if ([searchKind isEqualToString:SHOW_USER_KIND[0]]) {
		searchKind = PET_KIND_INFO[0];
	} else if ([searchKind isEqualToString:SHOW_USER_KIND[1]]){
		searchKind = PET_KIND_INFO[1];
	} else {
		searchKind = @"";
	}
	
	if ([searchGender isEqualToString:SHOW_USER_GENDER[0]]) {
		searchGender = PET_GENDER_INFO[0];
	} else if ([searchGender isEqualToString:SHOW_USER_GENDER[1]]){
		searchGender = PET_GENDER_INFO[1];
	} else {
		searchGender = @"";
	}
	
	if ([searchBodyType isEqualToString:SHOW_USER_BODYTYPE[0]]) {
		searchBodyType = [NSString stringWithFormat:@"%@%@", PET_BODYTYPE_INFO[0], PET_BODYTYPE_INFO[1]];
	} else if ([searchBodyType isEqualToString:SHOW_USER_BODYTYPE[1]]){
		searchBodyType = PET_BODYTYPE_INFO[2];
	} else if ([searchBodyType isEqualToString:SHOW_USER_BODYTYPE[2]]){
		searchBodyType = PET_BODYTYPE_INFO[3];
	} else {
		searchBodyType = @"";
	}
	
	if ([searchAge isEqualToString:SHOW_USER_AGE[0]]) {
		searchAge = PET_AGE_INFO[0];
	} else if ([searchAge isEqualToString:SHOW_USER_AGE[1]]){
		searchAge = PET_AGE_INFO[1];
	} else {
		searchAge = @"";
	}
	
	searchClour = _clourLabel.text;
	searchLocation = _locationLabel.text;
	
	NSLog(@"搜尋條件：%@, %@, %@, %@, %@, %@", searchGender, searchKind, searchAge, searchClour, searchBodyType, searchLocation);
	
}

/// 分析篩選符合搜尋標準的資料
- (void)analysisAdoptionForSearch: (NSMutableArray *)analysisAdoption {
	for (NSDictionary *tmpAdoptInfo in analysisAdoption) {
		
		BOOL isKindSame = true;
		BOOL isGenderSame = true;
		BOOL isBodyTypeSame = true;
		BOOL isAgeSame = true;
		BOOL isClourSame = true;
		BOOL isLocationSame = true;
		
		if (![searchKind isEqualToString:@""]) {
			isKindSame = [tmpAdoptInfo[PET_KIND] isEqualToString:searchKind];
		}
		
		if (![searchGender isEqualToString:@""]) {
			isGenderSame = [tmpAdoptInfo[PET_GENDER] isEqualToString:searchGender];
		}
		
		if (![searchBodyType isEqualToString:@""]) {
			if ([searchBodyType isEqualToString:@"MINISMALL"]) {
				isBodyTypeSame = [tmpAdoptInfo[PET_BODY_SIZE] isEqualToString:PET_BODYTYPE_INFO[0]] || [tmpAdoptInfo[PET_BODY_SIZE] isEqualToString:PET_BODYTYPE_INFO[1]];
			} else {
				isBodyTypeSame = [tmpAdoptInfo[PET_BODY_SIZE] isEqualToString:searchBodyType];
			}
		}
		
		if (![searchAge isEqualToString:@""]) {
			isAgeSame = [tmpAdoptInfo[PET_AGE] containsString:searchAge];
		}
		
		if (![searchLocation isEqualToString:PET_LOCATION_ARRAY[0]]) {
			isLocationSame = [tmpAdoptInfo[PET_SHELTER] containsString:searchLocation];
		}
		
		if (![searchClour isEqualToString:PET_CLOUR_ARRAY[0]]) {
			
			NSString *tmpPetClour = tmpAdoptInfo[PET_COLOUR];
			BOOL isColorASame = true;
			BOOL isColorBSame = true;
			
			if ([searchClour isEqualToString:PET_CLOUR_ARRAY[1]]) {
				isColorASame = [tmpPetClour containsString:@"米"];
				isColorBSame = [tmpPetClour containsString:@"白"];
			} else if ([searchClour isEqualToString:PET_CLOUR_ARRAY[2]]) {
				isColorASame = [tmpPetClour containsString:@"橘"];
				isColorBSame = [tmpPetClour containsString:@"黃"];
			} else if ([searchClour isEqualToString:PET_CLOUR_ARRAY[3]]) {
				isColorASame = [tmpPetClour containsString:@"棕"];
				isColorBSame = [tmpPetClour containsString:@"咖啡"];
			} else if ([searchClour isEqualToString:PET_CLOUR_ARRAY[4]]) {
				isColorASame = [tmpPetClour containsString:@"黑"];
				isColorBSame = nil;
			} else if ([searchClour isEqualToString:PET_CLOUR_ARRAY[5]]) {
				isColorASame = [tmpPetClour containsString:@"虎"];
				isColorBSame = nil;
			} else if ([searchClour isEqualToString:PET_CLOUR_ARRAY[6]]) {
				isColorASame = [tmpPetClour containsString:@"花"];
				isColorBSame = nil;
			}
			
			if ((!isColorBSame && isColorASame) || (isColorASame || isColorBSame)) {
				isClourSame = true;
			} else {
				isClourSame = false;
			}
		}
		
		// 符合以上所有條件者，塞入用以過濾搜尋的新字典
		if (isKindSame && isGenderSame && isBodyTypeSame && isAgeSame && isLocationSame && isClourSame) {
			[searchAdoptInfo addObject:tmpAdoptInfo];
		}
	}
}

#pragma mark - Table view data sources

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	
	// 判斷pkview的顯示
	if (indexPath.section==0 && (indexPath.row==4 ||indexPath.row==6)) {
		
		if (showRowFirstSec != indexPath.row+1) {
			showRowFirstSec = indexPath.row+1;
		} else {
			showRowFirstSec = 0;
		}
	}
	
	[self.tableView beginUpdates];
	[self.tableView endUpdates];
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	// 判斷pkview的顯示
	if (indexPath.section==0 && ((indexPath.row==5 && indexPath.row != showRowFirstSec) ||
								 (indexPath.row==7 && indexPath.row != showRowFirstSec))) {
		return 0;
	}
	return [super tableView:tableView heightForRowAtIndexPath:indexPath];
}

#pragma mark - Picker View Data Sources

// 設定每個pickerview中有幾個欄位
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
	return 1;
}

// 設定每個pickerView中會有幾列
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {

	if (pickerView == _clourPickerView) {
		return pickerClour.count;
	}
	
	if (pickerView == _locationPickerView) {
		return pickerLocation.count;
	}
	
	return 0;
}

// 設定pickerview中每列要呈現的資訊
-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
	
	if (pickerView == _clourPickerView) {
		return pickerClour[row];
	}
	
	if (pickerView == _locationPickerView) {
		return pickerLocation[row];
	}
	
	return @"";
}

// 根據使用者滑動pickerview的結果，顯示對應的資訊在label上
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
	
	if (pickerView == _clourPickerView) {
		_clourLabel.text = pickerClour[row];
		clourPVRow = row;
	}
	
	if (pickerView == _locationPickerView) {
		_locationLabel.text = pickerLocation[row];
		locationPVR = row;
	}
}

@end
