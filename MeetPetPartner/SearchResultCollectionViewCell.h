//
//  SearchResultCollectionViewCell.h
//  MeetPetPartner
//
//  Created by Yvonne.H on 2017/2/24.
//  Copyright © 2017年 YvonneH. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchResultCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *petPhoto;
@property (weak, nonatomic) IBOutlet UIImageView *kindGenderImage;
@property (weak, nonatomic) IBOutlet UILabel *shelterNameLabel;
@property (weak, nonatomic) IBOutlet UIButton *addFavoriteBtn;
@property (weak, nonatomic) IBOutlet UIButton *deleteFavoriteBtn;
@property (weak, nonatomic) IBOutlet UILabel *bodyTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *ageLabel;
@property (weak, nonatomic) IBOutlet UILabel *sterilizationLabel;
@property (weak, nonatomic) IBOutlet UILabel *bacterinLabel;

@end
