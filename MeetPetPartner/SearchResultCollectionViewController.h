//
//  SearchResultCollectionViewController.h
//  MeetPetPartner
//
//  Created by Yvonne.H on 2017/2/23.
//  Copyright © 2017年 YvonneH. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchResultCollectionViewController : UICollectionViewController
@property NSMutableArray *searchAdoptResult;
@end
