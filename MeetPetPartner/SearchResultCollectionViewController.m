//
//  SearchResultCollectionViewController.m
//  MeetPetPartner
//
//  Created by Yvonne.H on 2017/2/23.
//  Copyright © 2017年 YvonneH. All rights reserved.
//
#import "DataManager.h"
#import "SearchResultCollectionViewController.h"
#import "SearchResultCollectionViewCell.h"
#import "AdoptionInfoViewController.h"
#import <UIImageView+WebCache.h>

@interface SearchResultCollectionViewController () {
	DataManager *dataManager;
	NSMutableArray *userFavorite;
	SearchResultCollectionViewCell *searchResultCell;
}

@end

@implementation SearchResultCollectionViewController

static NSString * const reuseIdentifier = @"SearchResultCell";

- (void)viewDidLoad {
    [super viewDidLoad];
	dataManager = [DataManager sharedInstance];
	UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
								   initWithTitle:@""
								   style:UIBarButtonItemStylePlain
								   target:nil
								   action:nil];
	self.navigationItem.backBarButtonItem = backButton;
}

-(void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
	[dataManager loadSetting];
	userFavorite = [NSMutableArray new];
	userFavorite = dataManager.userFavoriteAdoption;
	[self.collectionView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
	[dataManager deleteSDWebImageCache];
}



#pragma mark - IBAction Method
/// 加入收藏的方法
- (IBAction)addFavoriteBtnPressed:(UIButton*)sender {
	// 取得indexpath
	NSIndexPath *indexPath = nil;
	indexPath = [self.collectionView indexPathForItemAtPoint:[self.collectionView convertPoint:sender.center fromView:sender.superview]];
	[userFavorite addObject:_searchAdoptResult[indexPath.row][PET_ID]];
	// 將變動的資料寫進userdefault
	[dataManager writeSetting:userFavorite forKey:USER_SETTING_FAVORITE];
	// 切換到main queue做介面更動
	dispatch_async(dispatch_get_main_queue(), ^{
		searchResultCell.addFavoriteBtn.hidden = true;
		searchResultCell.deleteFavoriteBtn.hidden = false;
	[self.collectionView reloadData];
	});
}

/// 取消收藏的方法
- (IBAction)deleteFavoriteBtnPressed:(UIButton *)sender {
	// 取得indexpath
	NSIndexPath *indexPath = nil;
	indexPath = [self.collectionView indexPathForItemAtPoint:[self.collectionView convertPoint:sender.center fromView:sender.superview]];
	[userFavorite removeObject:_searchAdoptResult[indexPath.row][PET_ID]];
	// 將變動的資料寫進userdefault
	[dataManager writeSetting:userFavorite forKey:USER_SETTING_FAVORITE];
	
	// 切換到main queue做介面更動
	dispatch_async(dispatch_get_main_queue(), ^{
		searchResultCell.addFavoriteBtn.hidden = false;
		searchResultCell.deleteFavoriteBtn.hidden = true;
		[self.collectionView reloadData];
	});
}

#pragma mark - UICollectionViewDataSource Methods

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _searchAdoptResult.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
	
	searchResultCell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
	
	// 設置cell的外觀
	searchResultCell.layer.masksToBounds = NO;
	searchResultCell.layer.borderColor = [UIColor grayColor].CGColor;
	searchResultCell.layer.contentsScale = [UIScreen mainScreen].scale;
	searchResultCell.layer.shadowOpacity = 0.4f;
	searchResultCell.layer.shadowRadius = 1.0f;
	searchResultCell.layer.shadowOffset = CGSizeZero;
	searchResultCell.layer.shadowPath = [UIBezierPath bezierPathWithRect:searchResultCell.bounds].CGPath;

	// 要顯示在cell上元件設定
	searchResultCell.shelterNameLabel.text = _searchAdoptResult[indexPath.row][PET_SHELTER];
	
	// 設定體型顯示
	searchResultCell.bodyTypeLabel.text = [DataManager showUserBodyTypeInfo:_searchAdoptResult[indexPath.row][PET_BODY_SIZE]];
	
	// 設定年紀顯示
	searchResultCell.ageLabel.text = [DataManager showUserAgeInfo:_searchAdoptResult[indexPath.row][PET_AGE]];
	
	// 設定絕育顯示
	searchResultCell.sterilizationLabel.text = [DataManager showUserSterilizationInfo:_searchAdoptResult[indexPath.row][PET_STERILIZATION]];
	
	// 設定疫苗施打顯示
	searchResultCell.bacterinLabel.text = [DataManager showUserBacterinInfo:_searchAdoptResult[indexPath.row][PET_BACTERIN]];
	
	// 設定收藏按鈕是否隱藏
	if ([userFavorite containsObject:_searchAdoptResult[indexPath.row][PET_ID]]) {
		searchResultCell.addFavoriteBtn.hidden =true;
		searchResultCell.deleteFavoriteBtn.hidden = false;
	} else {
		searchResultCell.addFavoriteBtn.hidden =false;
		searchResultCell.deleteFavoriteBtn.hidden = true;
	}
	
	// 設定照片顯示
	if ([_searchAdoptResult[indexPath.row][PET_PHOTO_URL] isEqualToString:@""]) {
		if ([_searchAdoptResult[indexPath.row][PET_KIND] isEqualToString:PET_KIND_INFO[0]]) {
			searchResultCell.petPhoto.image = [UIImage imageNamed:@"CatNoImage"];
		} else {
			searchResultCell.petPhoto.image = [UIImage imageNamed:@"DogNoImage"];
		}
	} else {
	NSURL *imgURL = [NSURL URLWithString:_searchAdoptResult[indexPath.row][PET_PHOTO_URL]];
	[searchResultCell.petPhoto sd_setImageWithURL:imgURL placeholderImage:[UIImage imageNamed:@"ImageLoading"]];
	}

	// 設定種類與性別顯示
	if ([_searchAdoptResult[indexPath.row][PET_KIND] isEqualToString:PET_KIND_INFO[0]]) {
		
		if ([_searchAdoptResult[indexPath.row][PET_GENDER] isEqualToString:PET_GENDER_INFO[0]]) {
			searchResultCell.kindGenderImage.image = [UIImage imageNamed:@"catGirl"];
		} else if ([_searchAdoptResult[indexPath.row][PET_GENDER] isEqualToString:PET_GENDER_INFO[1]]) {
			searchResultCell.kindGenderImage.image = [UIImage imageNamed:@"catBoy"];
		} else {
			searchResultCell.kindGenderImage.image = [UIImage imageNamed:@"catUnknown"];
		}
		
	} else if ([_searchAdoptResult[indexPath.row][PET_KIND] isEqualToString:PET_KIND_INFO[1]]) {
		
		if ([_searchAdoptResult[indexPath.row][PET_GENDER] isEqualToString:PET_GENDER_INFO[0]]) {
			searchResultCell.kindGenderImage.image = [UIImage imageNamed:@"dogGirl"];
		} else if ([_searchAdoptResult[indexPath.row][PET_GENDER] isEqualToString:PET_GENDER_INFO[1]]) {
			searchResultCell.kindGenderImage.image = [UIImage imageNamed:@"dogBoy"];
		} else {
			searchResultCell.kindGenderImage.image = [UIImage imageNamed:@"dogUnknown"];
		}
		
	}
    return searchResultCell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
	
	AdoptionInfoViewController *adoptionInfoPage = [self.storyboard instantiateViewControllerWithIdentifier:@"AdoptionInfoPage"];
	
	adoptionInfoPage.adoptionInfo = _searchAdoptResult[indexPath.row];
	
	[self showViewController:adoptionInfoPage sender:self];
	
}


#pragma mark - UICollectionViewDelegate Methods

// 設定cell的尺寸
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
	float cellWidth = ((self.view.frame.size.width) / 375.0f) * 350.0f;
	float cellHeight = ((self.view.frame.size.width) / 375.0f) * 185.0f;
	return CGSizeMake(cellWidth, cellHeight);
}

@end
