//
//  SettingTableViewController.m
//  MeetPetPartner
//
//  Created by Yvonne.H on 2017/3/4.
//  Copyright © 2017年 YvonneH. All rights reserved.
//

#import "SettingTableViewController.h"
#import "DataManager.h"
#import <SDImageCache.h>

// 應用推廣會使用到的固定字串
#define APP_SHARE_TEXT		@"浪浪回家APP：一個愛的機會，認養代替購買"

// APP的app store網址
#define APP_APPLESTORE_URL @"itms://itunes.apple.com/tw/app/id1212078175"

// 回報問題使用的固定字串
#define DEVELOPER_EMAIL		@"yuan_0723@icloud.com"
#define REPORT_SUBJECT		@"APP問題回報：浪浪回家"
#define REPORT_TEXT			@"iPhone機型： \n iOS版本號： \n\n 想要回報的事項："

// 阿呆比粉絲專業連結
#define ADEBBY_FACEBOOK_URL	@"https://www.facebook.com/Adebby0409"

// 農委會開放資訊平台網站
#define COA_GOV_WEB			@"http://data.coa.gov.tw/Default.aspx"

@interface SettingTableViewController () {
	DataManager *datamanager;
	NSNumber *indexPageShow;
}
@property (weak, nonatomic) IBOutlet UISegmentedControl *indexSettingSegment;

@end

@implementation SettingTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
	datamanager = [DataManager sharedInstance];

}

-(void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
	[datamanager loadSetting];
	indexPageShow = datamanager.indexPageShow;
	
	// 設置首頁segment預設值
	if ([indexPageShow isEqual:@0]) {
		[_indexSettingSegment setSelectedSegmentIndex:0];
	} else if ([indexPageShow isEqual:@1]) {
		[_indexSettingSegment setSelectedSegmentIndex:1];
	} else {
		[_indexSettingSegment setSelectedSegmentIndex:2];
	}
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
	[datamanager deleteSDWebImageCache];
}

#pragma mark - IBAction Methods
- (IBAction)indexSettingChanged:(UISegmentedControl *)sender {
	
	switch ([sender selectedSegmentIndex]) {
		case 0:
			indexPageShow = @(0);
			[datamanager writeSetting:indexPageShow forKey:USER_SETTING_INDEXPAGESHOW];
			break;
			
		case 1:
			indexPageShow = @(1);
			[datamanager writeSetting:indexPageShow forKey:USER_SETTING_INDEXPAGESHOW];
			break;
			
		case 2:
			indexPageShow = @(2);
			[datamanager writeSetting:indexPageShow forKey:USER_SETTING_INDEXPAGESHOW];
			break;
			
		default:
			indexPageShow = @(2);
			[datamanager writeSetting:indexPageShow forKey:USER_SETTING_INDEXPAGESHOW];
			break;
	}
}

#pragma mark - Private Methods
-(void) deleteCache {
	[[SDImageCache sharedImageCache]clearMemory];
	[[SDImageCache sharedImageCache]clearDisk];
	[datamanager deleteSDWebImageCache];
}


#pragma mark - Table view data source
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	
	// 清除快取
	if (indexPath.section==0 && indexPath.row==1) {
		
		dispatch_async(dispatch_get_main_queue(), ^{
			UIAlertController * alert =[UIAlertController alertControllerWithTitle:@"注意！"
																		   message:@"確定要清除緩存快取嗎？"
																	preferredStyle:UIAlertControllerStyleAlert];
			
			//準備警告視窗上的取消按鈕
			UIAlertAction * cancel = [UIAlertAction actionWithTitle:@"取消"
															  style:UIAlertActionStyleCancel
															handler:^(UIAlertAction * _Nonnull action) {}];
			
			//準備警告視窗上的確認按鈕
			UIAlertAction * confirm = [UIAlertAction actionWithTitle:@"確認"
															   style:UIAlertActionStyleDefault
															 handler:^(UIAlertAction * _Nonnull action) {
																 [self deleteCache];
															 }];
			
			//將按鈕加到警告視窗上
			[alert addAction:cancel];
			[alert addAction:confirm];
			//顯示警告視窗
			[self presentViewController:alert animated:YES completion:nil];
		});
	}
	
	// 五星好評
	if (indexPath.section==1 && indexPath.row==0) {
		[DataManager launchURLSchemeAction:APP_APPLESTORE_URL];
	}
	
	// 應用推廣
	if (indexPath.section==1 && indexPath.row==1) {

		NSArray *activityItems=@[APP_SHARE_TEXT,[NSURL URLWithString:APP_SHARE_URL]];
		
		dispatch_async(dispatch_get_main_queue(), ^{
			UIActivityViewController *viewController=[[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
			// 過濾不想呈現在activity內的項目
			viewController.excludedActivityTypes=@[UIActivityTypePrint,UIActivityTypeAddToReadingList,UIActivityTypeCopyToPasteboard];
			[self presentViewController:viewController animated:YES completion:nil];
		});
	}
	
	// 問題回報
	if (indexPath.section==1 && indexPath.row==2) {
		NSString *mailURL = [NSString stringWithFormat:@"mailto://%@?subject=%@&body=%@", DEVELOPER_EMAIL, REPORT_SUBJECT, REPORT_TEXT];
		[DataManager launchURLSchemeAction:mailURL];
	}
	
	// 阿呆比粉絲專業
	if (indexPath.section==1 && indexPath.row==3) {
		[DataManager launchURLSchemeAction:ADEBBY_FACEBOOK_URL];
	}
	
	// 農委會開放平台
	if (indexPath.section==1 && indexPath.row==4) {
		[DataManager launchURLSchemeAction:COA_GOV_WEB];
	}
}

@end
