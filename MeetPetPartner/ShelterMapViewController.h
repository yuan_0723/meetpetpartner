//
//  ShelterMapViewController.h
//  MeetPetPartner
//
//  Created by Yvonne.H on 2017/2/21.
//  Copyright © 2017年 YvonneH. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShelterMapViewController : UIViewController

//@property NSString *shelterAddress;
@property MKPlacemark *shelterPlacemark;
@property NSDictionary *adoptionInfo;

@end
