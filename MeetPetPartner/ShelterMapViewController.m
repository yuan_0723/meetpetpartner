//
//  ShelterMapViewController.m
//  MeetPetPartner
//
//  Created by Yvonne.H on 2017/2/21.
//  Copyright © 2017年 YvonneH. All rights reserved.
//

#import <MapKit/MapKit.h>
#import "DataManager.h"
#import "ShelterMapViewController.h"

@interface ShelterMapViewController ()<MKMapViewDelegate, CLLocationManagerDelegate>
{
	CLLocationManager *locationManager;
	CLLocation *placemarkCLLocation;
}
@property (weak, nonatomic) IBOutlet MKMapView *shelterMap;
@property (weak, nonatomic) IBOutlet UILabel *distanceLabel;

@end

@implementation ShelterMapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
	
	locationManager =[CLLocationManager new];
	[locationManager requestWhenInUseAuthorization];
	locationManager.delegate = self;
	
	//設定定位方式
	locationManager.delegate = self;
	locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
	locationManager.activityType = CLActivityTypeFitness; //定位更新的移動速度層級(再此設定為走路)
	[locationManager startUpdatingLocation];//開始回報使用者位置
	
	//在收容所的位置插上大頭針
	dispatch_async(dispatch_get_main_queue(),^{
		MKCoordinateRegion region = _shelterMap.region;
	
		region.center = [(CLCircularRegion *)_shelterPlacemark.region center];
		region.span.longitudeDelta /= 50;
		region.span.latitudeDelta /= 50;
	
		[_shelterMap setRegion:region animated:false];
		[_shelterMap addAnnotation:_shelterPlacemark];
	});
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - IBAction Methods
/// 按下導航按鈕後所觸發的方法
- (IBAction)mapNavigationBtnPressed:(UIBarButtonItem *)sender {
	
	UIAlertController *mapNavigationAlert = [UIAlertController alertControllerWithTitle:@"即將開啟導航" message:@"請選擇您偏好地圖應用程式。" preferredStyle:UIAlertControllerStyleActionSheet];
	
	UIAlertAction *appleMap = [UIAlertAction actionWithTitle:@"Apple地圖" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
		
		NSLog(@"使用者按下了蘋果地圖");
		[self openAppleMap];
	}];
	
	UIAlertAction *googleMap = [UIAlertAction actionWithTitle:@"Google地圖" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
		
		NSLog(@"使用者按下了GOOGLE地圖");
		[self openGoogleMap];
	}];
	
	UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
	
	[mapNavigationAlert addAction:appleMap];
	[mapNavigationAlert addAction:googleMap];
	[mapNavigationAlert addAction:cancel];
	
	[self presentViewController:mapNavigationAlert animated:YES completion:nil];
}

#pragma mark - Private Methods

/// 執行蘋果內建地圖的方法
-(void)openAppleMap {
	
	// 取得現在所在位置
	MKMapItem *currentLocation = [MKMapItem mapItemForCurrentLocation];
	
	// 跟據傳值過來的placemark創建出mkmapitem
	MKMapItem *shelterLocation = [[MKMapItem alloc] initWithPlacemark:_shelterPlacemark];
	// 設定大頭針上的標籤資訊
	shelterLocation.name = _adoptionInfo[PET_SHELTER];
	shelterLocation.phoneNumber = _adoptionInfo[PET_SHELTER_TEL];
	
	// 決定導航的起點跟終點
	NSArray *destination = [[NSArray alloc] initWithObjects:currentLocation, shelterLocation, nil];
	
	// 設定導航模式
	NSDictionary *navigationMode = [NSDictionary dictionaryWithObject:MKLaunchOptionsDirectionsModeDriving forKey:MKLaunchOptionsDirectionsModeKey];
	
	// 開啓apple內建的地圖
	[MKMapItem openMapsWithItems:destination launchOptions:navigationMode];
}

/// 執行google地圖的方法
-(void)openGoogleMap {
	NSString *googlemapURL = [NSString stringWithFormat:@"http://maps.google.com/maps?q=%@",_adoptionInfo[PET_SHELTER_ADDRESS]];
	[DataManager launchURLSchemeAction:googlemapURL];
}

//#pragma mark - CLLocationManagerDelegate Method
//每次回報使用者位置時，自動觸發
-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations {

	CLLocation * currentLocation = locations.lastObject; //lastObject最新的位置
	CLLocationCoordinate2D coordinate = currentLocation.coordinate; //coordinate內含屬性可以知道緯經度
	
	NSLog(@"Current Location: %.6f, %.6f", coordinate.latitude,coordinate.longitude);
	
	
	CLLocationDistance placeDistance = [currentLocation distanceFromLocation:_shelterPlacemark.location];
	int distanceKM = (int)ceil(placeDistance/1000.0);
	_distanceLabel.text = [NSString stringWithFormat:@"與收容所距離大約：%d 公里", distanceKM];
	
//	static dispatch_once_t onceToken;
//	//宣告靜態的變數onceToken static:靜態（不會隨函式重啟使數值消失） //啟動時就自動配置記憶體，
//	dispatch_once(&onceToken, ^{ //保證某個 block 只會被執行一次
//		MKCoordinateSpan span = MKCoordinateSpanMake(0.005, 0.005); //會影響地圖的縮放大小，單位是經緯度，mapView會依照所在地自行做校正
//		MKCoordinateRegion region = MKCoordinateRegionMake(coordinate, span);
//		[_shelterMap setRegion:region animated:true];
//		region = _shelterMap.region;
//		
//		//放上大頭針
//		CLLocationCoordinate2D storeCoordinate = coordinate; //拿取現在的位置
//		storeCoordinate.latitude += 0.0005; //將緯經度以現在位址為基準增加0.0005
//		storeCoordinate.longitude += 0.0005;
//		
//		MKPointAnnotation * annotation = [MKPointAnnotation new]; //大頭針資料物件初始化
//		annotation.coordinate = storeCoordinate;
//		annotation.title=@"麥當勞";
//		annotation.subtitle=@"-比較好吃-";
//		
//		[_mainMapView addAnnotation:annotation];//送給mapview讓它顯示
//	});
}

@end
