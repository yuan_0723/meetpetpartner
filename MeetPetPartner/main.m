//
//  main.m
//  MeetPetPartner
//
//  Created by Yvonne.H on 2017/2/16.
//  Copyright © 2017年 YvonneH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
	@autoreleasepool {
	    return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
	}
}
